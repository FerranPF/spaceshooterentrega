﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyGameManager : MonoBehaviour {

	public Text highscoreText;
	public Text livesText;
	private int lives;
	public int highscore;

	public GameObject EscapeGraphics;

	private static MyGameManager instance;

	void Awake(){
		if (instance == null) {
			instance = this;
		}

		EscapeGraphics.SetActive (false);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Escape)) {
			EscapeGraphics.SetActive (true);
			Time.timeScale = 0;
		}
		if(lives == -1){
			GameOver ();
		}
	}

	public void Resume(){
		EscapeGraphics.SetActive (false);
		Time.timeScale = 1;
	}

	public void MainMenu(){
		SceneManager.LoadScene ("MainMenu");
		Time.timeScale = 1;
	}
	public static MyGameManager GetInstance(){
		return instance;
	}

	void Start () {
		lives = 3;
		highscore = 0;
		highscoreText.text = "0";
		livesText.text = "x " + lives.ToString();
	}

	public void LoseLive(){
		
		lives--;

		if (lives >= 0) {
			livesText.text = "x " + lives.ToString();
		}
	}

	public void GainHighscore(int points){
		highscore += points;
		highscoreText.text = highscore.ToString ();
	}

	public void QuitGame(){
		Application.Quit ();
	}

	public void GameOver(){
		SceneManager.LoadScene ("GameOver");
	}
}
