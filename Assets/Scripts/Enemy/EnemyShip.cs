﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyShip : MonoBehaviour {

	private Animator enemyAnim;
	private Collider2D enemyColl;
	private Renderer enemyGraph;
	public AudioSource enemyExpl;
	public ParticleSystem enemyParticle;

    void Awake(){
		enemyAnim = GetComponent<Animator> ();
		enemyColl = GetComponent<Collider2D> ();
		enemyGraph = GetComponent<Renderer> ();
    }

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.tag == "Finish") {
			Reset ();
		}
		if (coll.tag == "Bullet"){
			Explode ();
			MyGameManager.GetInstance().GainHighscore (30);
		}
	}

	public void Move(){
		enemyAnim.SetBool ("moving", true);
	}

	void Explode(){
		enemyExpl.Play ();
		enemyParticle.Play ();
		enemyColl.enabled = false;
		enemyGraph.enabled = false;
        Invoke ("Reset", 2.2f);
	}

	public void Reset(){
		enemyColl.enabled = true;
		enemyGraph.enabled = true;
		enemyAnim.SetBool ("moving", false);
	}
}