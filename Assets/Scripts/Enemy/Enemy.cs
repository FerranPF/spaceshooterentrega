﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	public EnemyShip enemy;
    public EnemyManager em;

    public void LaunchEnemy(Vector3 position)
    {
		transform.position = position;
        enemy.Move();
    }
}
