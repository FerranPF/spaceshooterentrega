﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCache {
	private Enemy[] enemies;
	private int currentEnemy = 0;


	public EnemyCache(EnemyManager em, GameObject prefabEnemy, Vector3 position, Transform parent, int numEnemies){
		enemies = new Enemy[numEnemies];

		Vector3 tmpPosition = position;
		for(int i=0;i<numEnemies;i++){
			enemies[i] = GameObject.Instantiate (prefabEnemy, tmpPosition, Quaternion.identity, parent).GetComponent<Enemy>();
			enemies[i].name = prefabEnemy.name+"_enemy_" + i;
			enemies[i].em = em;
            enemies[i].transform.position = new Vector3 (tmpPosition.x, tmpPosition.y, tmpPosition.z);
			tmpPosition.x += 1;
		}
	}

	public Enemy GetEnemy(){
		if (currentEnemy > enemies.Length - 1) {
			currentEnemy = 0;
		}

		return enemies [currentEnemy++];
	}

	~EnemyCache()
	{

	}
}