﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour {

	public Transform launchPos;
	public Transform creationPos;
	public float timeLaunch;
	private int enemyChange;

	public GameObject[] enemyPrefabs;
	private EnemyCache[] enemyCaches;
	private float currentTime;

	private static EnemyManager instance;

	public static EnemyManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Start () {
		if (instance == null) {
			instance = this;
		}

		Vector3 creationPosEnemy = creationPos.position;
		enemyCaches = new EnemyCache[enemyPrefabs.Length];

		for (int i = 0; i < enemyPrefabs.Length; i++) {
			enemyCaches[i] = new EnemyCache (this, enemyPrefabs[i], creationPosEnemy, creationPos, 15);

			creationPosEnemy.y += 1;
		}

	}
    
	void Update(){
		currentTime += Time.deltaTime;
		enemyChange = Random.Range (0, enemyCaches.Length);
		if (currentTime > timeLaunch) {
			enemyCaches [enemyChange].GetEnemy ().LaunchEnemy (launchPos.position);
			currentTime -= timeLaunch;
		}
	}
    
}
