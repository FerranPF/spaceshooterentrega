﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorManager : MonoBehaviour {

	public Transform launchPos;
	public Transform creationPos;
	public float timeLaunch;
	private int meteorChange;

	public GameObject[] meteorPrefabs;
	private MeteorCache[] meteorCaches;
	private float currentTime;

	private static MeteorManager instance;

	public static MeteorManager getInstance(){
		return instance;
	}

	// Use this for initialization
	void Awake () {
		if (instance == null) {
			instance = this;
		}
		Vector3 creationPosMeteor = creationPos.position;
		meteorCaches = new MeteorCache[meteorPrefabs.Length];

		for (int i = 0; i < meteorPrefabs.Length; i++) {
			meteorCaches[i] = new MeteorCache (this, meteorPrefabs[i], creationPosMeteor, creationPos, 20);

			creationPosMeteor.y += 1;
		}
	}

	void Update(){
		currentTime += Time.deltaTime;
		meteorChange = Random.Range (0, meteorCaches.Length);
		if (currentTime > timeLaunch) {
			meteorCaches [meteorChange].GetMeteor ().LaunchMeteor (launchPos.position, new Vector2 (Random.Range(-5, 5), Random.Range(-5, -2)), Random.Range(-5, 5));
			currentTime -= timeLaunch;
		}
	}

	public void LaunchMeteor(int type, Vector3 position, Vector2 direction, float rotation){
		meteorCaches [type].GetMeteor ().LaunchMeteor (position, direction, rotation);
	}
}