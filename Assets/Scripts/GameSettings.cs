﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSettings : MonoBehaviour {

	public static GameSettings instance;

	void Start () {
		if (instance == null) {
			instance = this;	
			DontDestroyOnLoad (this.gameObject);
		}
	}

}
