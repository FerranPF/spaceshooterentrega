﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class SFXSetup : MonoBehaviour {

	public AudioMixer audioMixer;
	public Slider sfxVolume;

	void Awake () {
		sfxVolume = GetComponent<Slider> ();
	}

	void OnEnable(){
		float tmpvalue;
		audioMixer.GetFloat ("SFXVolume", out tmpvalue);
		sfxVolume.value = tmpvalue;
	}

}
