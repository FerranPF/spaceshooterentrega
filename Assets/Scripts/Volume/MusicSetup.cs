﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MusicSetup : MonoBehaviour {

	public Slider musicVolume;
	public AudioMixer audioMixer;
	void Awake () {
		musicVolume = GetComponent<Slider>();
	}

	void OnEnable(){
		float tmpvalue;
		audioMixer.GetFloat ("MusicVolume", out tmpvalue);
		musicVolume.value = tmpvalue;
	}

}
