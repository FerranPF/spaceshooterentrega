﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet_6 : Bullet {

	public int maxAmmo = 2;
	public float timeToChange = 1.0f;
	private float currentTime = 0f;
	private int dir = 90;

	protected new virtual void Update(){
		base.Update ();
		if (shooting) {
			currentTime += Time.deltaTime;
			if (currentTime >= timeToChange) {
				transform.Rotate(new Vector3 (0, 0, dir));
				currentTime = 0f;
			}
		} else {
			currentTime = 0f;
		}
	}
}