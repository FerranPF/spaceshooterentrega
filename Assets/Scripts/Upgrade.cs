﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Upgrade : MonoBehaviour {

    private Vector3 spawnPos;

    private float timeToSpawn = 0f;
    public float spawnTime = 0f;
    public int speed;

    private Vector3 posIni;

    private bool moving = false;

    private void Awake()
    {
        posIni = new Vector3(Random.Range(-8f, 7.5f), 5.5f, 0);;
    }

    private void Start()
    {
        spawnPos = transform.position;
    }

    private void Update()
    {
        timeToSpawn += Time.deltaTime;

        if(timeToSpawn > spawnTime)
        {
            transform.position = posIni;
            timeToSpawn = 0f;
            moving = true;
        }

        if(moving)
        {
            transform.Translate(0, -speed * Time.deltaTime, 0);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player" || collision.tag == "Finish")
        {
            ResetUpgrade();
            transform.position = spawnPos;
        }
    }

    private void ResetUpgrade()
    {
        posIni = new Vector3(Random.Range(-8f, 7.5f), 5.5f, 0);
        timeToSpawn = 0f;
        moving = false;
    }
}
