﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveSpawn : MonoBehaviour {

	public int xMin;
	public int xMax;
	private int randPos;
	public int timeChange;
	private int cont;


	void Update () {
		cont++;
		if(cont >= timeChange){
			ChangePos ();
			cont = 0;
		}
	}

	void ChangePos(){
		randPos = Random.Range(xMin, xMax);

		transform.position = new Vector3 (randPos, transform.position.y, transform.position.z);
	}
}
