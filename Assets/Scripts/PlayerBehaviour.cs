﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour {
	public float speed;
	private Vector2 axis;
	public Vector2 limits;
	public Propeller propeller;
	public ParticleSystem explosion;
	public GameObject graphicsPlayer;
	public AudioSource explosionPlayer;
	public Weapons weapons;
	public CameraShake cameraShake;

	public Transform spawnPoint;
	public GameObject shipInput;
	private bool invincible;
	public GameObject graphicShield;
	private bool shield;

	public float duration = .15f;
	public float magnitude = .4f;

	void Start(){
		graphicShield.SetActive (false);
	}

	void Update () {
		transform.Translate (axis * speed * Time.deltaTime);

		if (transform.position.x > limits.x) {
			transform.position = new Vector3 (limits.x, transform.position.y, transform.position.z);
		}else if (transform.position.x < -limits.x) {
			transform.position = new Vector3 (-limits.x, transform.position.y, transform.position.z);
		}

		if (transform.position.y > limits.y) {
			transform.position = new Vector3 (transform.position.x, limits.y, transform.position.z);
		}else if (transform.position.y < -limits.y) {
			transform.position = new Vector3 (transform.position.x, -limits.y, transform.position.z);
		}

		if (axis.y > 0) {
			propeller.RedFire ();
		} else if (axis.y < 0) {
			propeller.BlueFire ();
		} else {
			propeller.Stop ();
		}
	}


	public void SetAxis(Vector2 currentAxis){
		axis = currentAxis;
	}


	public void OnTriggerEnter2D(Collider2D other){
		if(!invincible){
			
			if (other.tag == "Kill" && shield == false) {
				invincible = true;
				StartCoroutine(cameraShake.Shake(duration, magnitude));
				Explode ();
			}
			if (other.tag == "Enemy" && shield == false) {
				invincible = true;
				StartCoroutine(cameraShake.Shake(duration, magnitude));
				Explode ();
			}
			if (other.tag == "Bullet1") {
				weapons.Bullet1 ();
				Invoke ("ResetBullet", 6);
			}
			if (other.tag == "Bullet2") {
				weapons.Bullet2 ();
				Invoke ("ResetBullet", 6);
			}
			if (other.tag == "Shield") {
				shield = true;
				graphicShield.SetActive (true);
				Invoke ("resetInvulnerability", 5);
			}
		}
	}


	private void Explode (){
		MyGameManager.GetInstance ().LoseLive ();
		graphicsPlayer.SetActive (false);
		explosion.Play ();
		explosionPlayer.Play ();
		shipInput.SetActive (false);
		Invoke ("Reset", 1);
	}


	private void Reset(){
		transform.position = spawnPoint.position;
		graphicsPlayer.SetActive (true);
		shipInput.SetActive (true);
		graphicShield.SetActive (true);
		shield = true;
		Invoke ("resetInvulnerability", 2);
	}

	void resetInvulnerability(){
		shield = false;
		invincible = false;
		graphicShield.SetActive (false);
	}

	void ResetBullet(){
		weapons.DefaultBullet ();
	}
}
