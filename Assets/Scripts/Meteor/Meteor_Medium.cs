﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor_Medium : Meteor_Tiny {
	protected override void Explode(){

		MeteorManager.getInstance ().LaunchMeteor (2, transform.position, new Vector2 (-4, -4), -5);
		MeteorManager.getInstance ().LaunchMeteor (2, transform.position, new Vector2 (4, -4), -5);

		base.Explode ();
	}
}
