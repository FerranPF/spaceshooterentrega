﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Meteor : MonoBehaviour{
	
	public bool launched;
	public Vector2 moveSpeed;
	protected float rotationSpeed;
	public MeteorManager mm;

	public ParticleSystem particle;
	public GameObject graphicsMeteor;
	private Collider2D meteorCollider;
	private Transform meteorPos;

	public AudioSource audioExplosion;

	public Vector3 iniPos;

	void Start(){
		iniPos = transform.position;
		meteorCollider = GetComponent<Collider2D> ();
	}

	public void LaunchMeteor(Vector3 position,Vector2 direction, float rotation){
		transform.position = position;
		launched = true;
		transform.rotation = Quaternion.Euler(0, 0, 0);
		moveSpeed = direction;
		rotationSpeed = rotation;
	}

	protected void Update (){
		if (launched){
			transform.Translate (moveSpeed.x*Time.deltaTime, moveSpeed.y * Time.deltaTime, 0);
			transform.GetChild(0).Rotate (0, 0, rotationSpeed * Time.deltaTime * 20);
		}
	}

	protected virtual void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Bullet"){
			Explode ();
		}else if(other.tag == "Finish"){
			Reset ();
		}
	}

	protected void Reset(){
		transform.position = iniPos;
		launched = false;
		meteorCollider.enabled = true;
		graphicsMeteor.SetActive (true);
	}

	protected virtual void Explode(){
		MyGameManager.GetInstance().GainHighscore (5);

		audioExplosion.Play ();
		meteorCollider.enabled = false;
		launched = false;
		graphicsMeteor.SetActive (false);
		particle.Play ();
		Invoke ("Reset", 1);
	}
}