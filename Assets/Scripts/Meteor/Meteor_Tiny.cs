﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meteor_Tiny: Meteor {

	public GameObject[] customGraphics;

	void Awake(){
		int selected = Random.Range (0, customGraphics.Length);
		for (int i = 0; i < customGraphics.Length; i++) {
			if (i != selected) {
				customGraphics [i].SetActive (false);
			}
		}
		enabled = true;
	}
	protected override void Explode(){
		base.Explode ();
	}
}
