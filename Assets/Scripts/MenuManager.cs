﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class MenuManager : MonoBehaviour {

	public GameObject mainGraphics;
	public GameObject optionsGraphics;

	public AudioMixer audioMixer;

	private void Awake(){
		optionsGraphics.SetActive (false);
	}

	public void Back(){
		mainGraphics.SetActive (true);
		optionsGraphics.SetActive (false);
	}

	public void PlayGame(){
		SceneManager.LoadScene ("SpaceShooter");
	}

	public void Options(){
		mainGraphics.SetActive (false);
		optionsGraphics.SetActive (true);
	}

	public void Creditos(){
		SceneManager.LoadScene ("Creditos");
	}

	public void QuitGame(){
		Application.Quit ();
	}

	public void SetMusicVolume(float volume){
		audioMixer.SetFloat ("MusicVolume", volume);
	}

	public void SetSFXVolume(float volume){
		audioMixer.SetFloat ("SFXVolume", volume);
	}
}
